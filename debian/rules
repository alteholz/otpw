#!/usr/bin/make -f
# -*- makefile -*-
# Sample debian/rules that uses debhelper.
# This file was originally written by Joey Hess and Craig Small.
# As a special exception, when this file is copied by dh-make into a
# dh-make output file, you may use that output file without restriction.
# This special exception was added by Craig Small in version 0.37 of dh-make.

include /usr/share/dpkg/architecture.mk

# Uncomment this to turn on verbose mode.
#export DH_VERBOSE=1

HCFLAGS=$(shell dpkg-buildflags --get CFLAGS)
HCPPFLAGS=$(shell dpkg-buildflags --get CPPFLAGS)
HLDFLAGS=$(shell dpkg-buildflags --get LDFLAGS)

CFLAGS = -Wall -g

ifneq (,$(findstring noopt,$(DEB_BUILD_OPTIONS)))
	CFLAGS += -O0
else
	CFLAGS += -O2
endif

build-arch: build
build-indep: build

build: build-stamp
build-stamp:
	dh_testdir
	
	# Add here commands to compile the package.
	$(MAKE) CFLAGS="-fPIC -O -ggdb -W -Wall $(HCFLAGS) $(HCPPFLAGS)" LDFLAGS="$(HLDFLAGS) -Wl,-z,now" otpw-gen pam_otpw.so
	touch $@

clean:
	dh_testdir
	dh_testroot
	rm -f build-stamp configure-stamp
	
	# Add here commands to clean up after the build process.
	[ ! -f Makefile ] || $(MAKE) clean
	
	dh_clean

install: build
	dh_testdir
	dh_testroot
	dh_prep
	dh_installdirs
	
	# Add here commands to install the package into debian/tmp
	mkdir -p debian/tmp
	install -d debian/tmp/usr/bin
	install otpw-gen debian/tmp/usr/bin
	install -d debian/tmp/usr/lib/$(DEB_HOST_MULTIARCH)/security
	install pam_otpw.so debian/tmp/usr/lib/$(DEB_HOST_MULTIARCH)/security
	install -d debian/tmp/usr/include
	install -m644 otpw.h debian/tmp/usr/include
	install -d debian/tmp/usr/lib

# Build architecture-independent files here.
binary-indep: build install
# We have nothing to do by default.

# Build architecture-dependent files here.
binary-arch: build install
	dh_testdir
	dh_testroot
	dh_installchangelogs CHANGES
	dh_installdocs
	dh_installman
	dh_install --sourcedir=debian/tmp
	dh_strip
	dh_compress
	dh_fixperms
	dh_installdeb
	dh_shlibdeps
	dh_gencontrol
	dh_md5sums
	dh_builddeb

binary: binary-indep binary-arch
.PHONY: build clean binary-indep binary-arch binary install configure
